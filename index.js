
const FIRST_NAME = "Alexandra";
const LAST_NAME = "Smeada";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
   var webPage={};
   webPage.home=0;
   webPage.contact=0;
   webPage.count=0;
   webPage.about=0;


   function pageAccessCounter(value=''){

       switch(value){
           case 'ABOUT':
               value='about';
               break;
               case 'CONTACT':
               value='contact';
               
            
       }

       if(value === 'about'){
           webPage.about++;
           webPage.count++;
       }else if(value === 'contact'){
           webPage.contact++;
           webPage.count++;
       }else if(value === ''){
           webPage.home++;
           webPage.count++;
       }

       return webPage;
   }

   function getCache(){

    return webPage;
   }

   return {
       pageAccessCounter,
       getCache
   };
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

